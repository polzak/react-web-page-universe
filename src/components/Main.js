import React from 'react';

const MainImage = () => {
  return <section className="main-frame">
    <h3>We can see ourselves.</h3>
    <h4>From our universe.</h4>
  </section>
}

const Stories = () => {
  return <section>
    <article className="sun">
      <h4>The Sun</h4> 
      <p>Here comes the Sun.</p>
    </article>
    <article className="moon">
      <h4>The Moon</h4>
      <p>Aim for the Moon.</p>
    </article>
    <article className="earth">
      <h4>The Earth</h4>
      <p>The Earth laughs in flowers.</p>
    </article>
  </section>
}

const Main = () => {
    return <main>
      <MainImage />
      <Stories />
    </main>
  }

export default Main;
  